public class Cat{
  // Class und Instance fields 
  // Attribute welche den Status der Klasse oder des Objektes speichern
  
  private String fellfarbe;		// Instance fields --> gehören zum Objekt
  private int alter;			//   "		"		" 
  private String name;		 	//   "		"		"
  
  private static String rasse;  //  Class field  --> es gehört zur Klasse 


  // Class und Instance Methods 
  // Methoden bilden das Verhalten der Klasse oder des Objektes ab

  public static void main(String [] args){
	Cat katze = new Cat();		// KatzenObjekt 1
	Cat kater = new Cat();		// KatzenObjekt 2
	
	katze.name = "Blue";
	katze.alter = 2;
	katze.fellfarbe = "Russisch Blau";
	katze.rasse = "Karthaeuser";
	
	kater.name = "Tormaz";
	kater.alter = 5;
	kater.fellfarbe = "Glueckskatze";
	kater.rasse = "Hauskatze";
	
	System.out.println("---  Werte Katze -------");
	System.out.println("Name : " + katze.name);
	System.out.println("Alter : " + katze.alter);
	System.out.println("Farbe : " + katze.fellfarbe);
	System.out.println("Rasse : " + katze.rasse);
	System.out.println();
	System.out.println("---  Werte Kater -------");
	System.out.println("Name : " + kater.name);
	System.out.println("Alter : " + kater.alter);
	System.out.println("Farbe : " + kater.fellfarbe);
	System.out.println("Rasse : " + kater.rasse);
  }
  
  public void run(Cat katzenObjekt){
    System.out.println ("Katze"+ katzenObjekt.name + " läuft");
  }
  
  public void eat(int menge){
    System.out.println("Katze frist: " + menge );
  }






}